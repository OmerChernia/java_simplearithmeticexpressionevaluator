package com.example;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Scanner;

public class ArithmeticApp {

    // Converts a number string from the given base to base 10 (decimal)
    public static String convertBase(String input, int base) {
        BigInteger value = new BigInteger(input, base);
        String result = value.toString(10);
        return result;
    }

    // Splits an arithmetic expression into tokens of operands and operators
    public static String[] splitExpression(String expression) {
        List<String> tokens = new ArrayList<>();
        int i = 0;
        while (i < expression.length()) {
            char ch = expression.charAt(i);
            if (Character.isLetterOrDigit(ch)) {
                // Build a string of alphanumeric characters (operand)
                StringBuilder operand = new StringBuilder();
                while (i < expression.length() && (Character.isLetterOrDigit(ch) || ch == '.')) {
                    operand.append(ch);
                    i++;
                    if (i < expression.length()) {
                        ch = expression.charAt(i);
                    }
                }
                tokens.add(operand.toString());
            } else {
                // Add the operator as a separate token
                tokens.add(String.valueOf(ch));
                i++;
            }
        }
        return tokens.toArray(new String[tokens.size()]);
    }

    // Converts all operand tokens in the split expression to base 10
    public static String[] convertEvenIndexesToBase10(String[] splitArray, int base) {
        String[] convertedArray = new String[splitArray.length];
        for (int i = 0; i < splitArray.length; i++) {
            if (i % 2 == 0) { // Check if index is even
                convertedArray[i] = convertBase(splitArray[i], base);
            } else {
                convertedArray[i] = splitArray[i]; // Leave operators unchanged
            }
        }
        return convertedArray;
    }

    // Joins tokens back into a single expression string
    public static String convertToExpression(String[] tokens) {
        StringBuilder expression = new StringBuilder();
        for (String token : tokens) {
            expression.append(token);
        }
        return expression.toString();
    }

    // Converts a decimal number to the specified base
    public static String convertToBase(int number, int base) {
        return Integer.toString(number, base).toUpperCase(); // Convert to uppercase for bases larger than 10
    }

    // Checks if the expression is valid (no consecutive operators)
    private static boolean isValidExpression(String expression) {
        for (int i = 0; i < expression.length() - 1; i++) {
            char current = expression.charAt(i);
            char next = expression.charAt(i + 1);
            if ((current == '+' || current == '-' || current == '*' || current == '/') &&
                    (next == '+' || next == '-' || next == '*' || next == '/')) {
                return false; // Consecutive operators found
            }
        }
        return true; // No consecutive operators found
    }

    // Evaluates a mathematical expression in string form
    public static int evaluateExpression(String expression) {
        Stack<Integer> operands = new Stack<>();
        Stack<Character> operators = new Stack<>();

        for (int i = 0; i < expression.length(); i++) {
            char ch = expression.charAt(i);

            if (ch == ' ') {
                continue;
            } else if (Character.isDigit(ch)) {
                int operand = 0;
                while (i < expression.length() && Character.isDigit(expression.charAt(i))) {
                    operand = operand * 10 + (expression.charAt(i) - '0');
                    i++;
                }
                i--;
                operands.push(operand);
            } else if (ch == '(') {
                operators.push(ch);
            } else if (ch == ')') {
                while (operators.peek() != '(') {
                    processOperator(operands, operators);
                }
                operators.pop(); // Remove '('
            } else if (ch == '+' || ch == '-' || ch == '*' || ch == '/') {
                while (!operators.isEmpty() && precedence(operators.peek()) >= precedence(ch)) {
                    processOperator(operands, operators);
                }
                operators.push(ch);
            }
        }

        while (!operators.isEmpty()) {
            processOperator(operands, operators);
        }

        return operands.pop();
    }

    // Processes the operator and applies it to the top two operands in the stack
    private static void processOperator(Stack<Integer> operands, Stack<Character> operators)
            throws ArithmeticException {
        char operator = operators.pop();
        int operand2 = operands.pop();
        int operand1 = operands.pop();
        int result;
        switch (operator) {
            case '+':
                result = operand1 + operand2;
                break;
            case '-':
                result = operand1 - operand2;
                break;
            case '*':
                result = operand1 * operand2;
                break;
            case '/':
                if (operand2 == 0) {
                    throw new ArithmeticException(
                            "Error: trying to divide by 0 (evaluated: \"" + operand2 + "\")");
                }
                result = operand1 / operand2;
                break;
            default:
                throw new IllegalArgumentException("Invalid operator");
        }
        operands.push(result);
    }

    // Determines the precedence of operators
    private static int precedence(char operator) {
        switch (operator) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return 0;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int base;

        // Prompt for base until a valid base is entered
        do {
            System.out.println("Enter base (2/8/10/16): ");
            while (!scanner.hasNextInt()) {
                System.out.println("Error - Invalid input. Enter base (2/8/10/16): ");
                scanner.next();
            }
            base = scanner.nextInt();
            if (base != 2 && base != 8 && base != 10 && base != 16) {
                System.out.print("Error - this base isn’t supported. Please ");
            }
        } while (base != 2 && base != 8 && base != 10 && base != 16);

        scanner.nextLine();

        // Prompt for the arithmetic expression
        System.out.println("Enter expression: ");
        String expression = scanner.nextLine();
        String noSpacesExpression = expression.replaceAll("\\s+", "");
        String newExpression = "";

        // Validate the expression
        if (!isValidExpression(noSpacesExpression)) {
            System.out.println("Error: invalid expression: \"" + "" + "\"");
        } else {
            newExpression = convertToExpression(
                    convertEvenIndexesToBase10(splitExpression(noSpacesExpression), base));
            try {
                // Evaluate the expression and print the result
                Integer calcRes = evaluateExpression(newExpression);
                System.out.println("The value of expression " + expression + " is : " + convertToBase(calcRes, base));
            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());
            }
        }

        scanner.close();
    }
}