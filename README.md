```markdown
# Arithmetic Expression Evaluator

This project is a Java application that evaluates arithmetic expressions in various bases (2, 8, 10, 16). The application reads an arithmetic expression from the user, converts the operands to base 10, evaluates the expression considering operator precedence, and then converts the result back to the specified base.

## Features
- Supports bases: 2 (binary), 8 (octal), 10 (decimal), 16 (hexadecimal).
- Handles basic arithmetic operations: addition (+), subtraction (-), multiplication (*), and division (/).
- Validates expressions to ensure correct syntax.
- Displays detailed error messages for invalid inputs and unsupported bases.

## Getting Started

### Prerequisites
- Java Development Kit (JDK) 17 or higher
- Maven
- IntelliJ IDEA or any other Java IDE

### Installation

1. **Clone the repository:**
    ```sh
    git clone https://gitlab.com/OmerChernia/java_simplearithmeticexpressionevaluator.git
    cd ArithmeticExpressionEvaluator
    ```

2. **Open the project in IntelliJ IDEA:**
    - Open IntelliJ IDEA.
    - Select "File" > "Open" and choose the cloned repository directory.
    - Wait for IntelliJ IDEA to import the Maven project.

3. **Build the project:**
    - Open the Terminal in IntelliJ IDEA and run:
      ```sh
      mvn clean install
      ```

### Running the Application

1. **Run the application:**
    - In IntelliJ IDEA, right-click on `ArithmeticApp` class in the `src/main/java/com/example` directory.
    - Select "Run 'ArithmeticApp.main()'".

2. **Follow the prompts:**
    - Enter the base (2/8/10/16) when prompted.
    - Enter the arithmetic expression when prompted.

### Example

#### Input:

Enter base (2/8/10/16): 16
Enter expression: A00 - 1B * 11 / 9 + 3D


#### Output:
The value of expression A00 - 1B * 11 / 9 + 3D is : A0A

```
### Project Structure
ArithmeticExpressionEvaluator/
├── pom.xml
└── src
    └── main
        └── java
            └── com
                └── example
                    └── ArithmeticApp.java
```

### Contributing

1. Fork the repository.
2. Create your feature branch (`git checkout -b feature/YourFeature`).
3. Commit your changes (`git commit -m 'Add some feature'`).
4. Push to the branch (`git push origin feature/YourFeature`).
5. Open a pull request.


### Acknowledgements

- [Java Documentation](https://docs.oracle.com/en/java/javase/17/docs/api/index.html)
- [Maven](https://maven.apache.org/)
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)